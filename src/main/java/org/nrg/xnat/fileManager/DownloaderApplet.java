/*
 * file-downloader: org.nrg.xnat.fileManager.DownloaderApplet
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.fileManager;

import netscape.javascript.JSException;
import netscape.javascript.JSObject;
import org.apache.commons.lang3.StringEscapeUtils;
import org.nrg.framework.net.JSESSIONIDCookie;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.bean.CatEntryBean;
import org.nrg.xdat.bean.CatEntryMetafieldBean;
import org.nrg.xdat.bean.base.BaseElement;
import org.nrg.xdat.bean.reader.XDATXMLReader;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DownloaderApplet extends Applet {
    private       Label        _status           = new Label("Initializing session...");
    private       Label        _caption          = new Label("Overall progress:");
    private       Label        _localFolderLabel = new Label("Select local folder:");
    private       TextField    _localFolderEdit  = new TextField();
    private       Button       _browseButton     = new Button("Browse...");
    private       JProgressBar _progress         = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
    private       Button       _startButton      = new Button("Start");
    private final int          DOWNLOAD_STOPPED  = 0, DOWNLOAD_FINISHED = 1, DOWNLOAD_PROGRESS = 2;
    private final int SESSION_EMPTY    = 0;
    private final int SESSION_FINISHED = 2;
    private       int _downloadStatus  = DOWNLOAD_STOPPED; //file download status
    private       int _sessionStatus   = SESSION_EMPTY; //session download status

    private final InitSessionThread _initSessionThread = new InitSessionThread();
    private final Downloader        _downloader        = new Downloader();

    private final List<DownloadFileDescriptor> _files           = new ArrayList<>();
    private       File                         _saveDirectory   = null; //save directory
    private       long                         _totalSize       = 0;
    private       long                         _totalCount      = 0;
    private       long                         _fileCount       = 1;
    private       long                         _compressedTotal = 0, _uncompressedTotal = 0;
    private double                 _compressionRatio = 3.0;
    private DownloadFileDescriptor _currentFile      = null;
    private JSESSIONIDCookie _jsessionidCookie;
    private int              _stayinAliveInterval;
    private boolean          _showConsoleMessages;

    /**
     * ***************************************************************************
     * Start main thread, initialize graphical components
     */
    public void init() {
        //start the main downloading loop
        _downloadStatus = DOWNLOAD_STOPPED;

        console("init: file-downloader : 1.6");
        this.setBackground(Color.white);
        GridBagLayout      layout      = new GridBagLayout();
        GridBagConstraints constraints = new GridBagConstraints();
        this.setLayout(layout);
        Font lightFont = new Font("Verdana", Font.PLAIN, 11);
        Font boldFont  = new Font("Verdana", Font.BOLD, 11);

        int vertical = 5, left = 5, right = 5, gap = 5;

        //1st line
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.insets = new Insets(vertical, left, gap, right); //(top,left,bottom,right)
        _status.setFont(boldFont);
        _status.setAlignment(Label.LEFT);
        AddPanelComponent(_status, layout, constraints);

        //2nd line
        constraints.gridwidth = 1;
        constraints.weightx = 0;
        constraints.gridy = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = new Insets(0, left, gap, 0);
        _localFolderLabel.setFont(lightFont);
        _localFolderLabel.setAlignment(Label.LEFT);
        AddPanelComponent(_localFolderLabel, layout, constraints);

        constraints.gridwidth = GridBagConstraints.RELATIVE;
        constraints.weightx = 5;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = new Insets(0, 0, gap, 5);
        _localFolderEdit.setFont(lightFont);
        AddPanelComponent(_localFolderEdit, layout, constraints);

        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.weightx = 0;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = new Insets(0, 0, gap + 2, right);
        _browseButton.setFont(boldFont);
        AddPanelComponent(_browseButton, layout, constraints);
        _browseButton.addActionListener(new DownloadButtonListener());

        //3rd line
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 0;
        constraints.gridwidth = 1;
        constraints.gridy = 3;
        constraints.insets = new Insets(0, left, vertical, 0);
        _caption.setFont(lightFont);
        _caption.setAlignment(Label.LEFT);
        AddPanelComponent(_caption, layout, constraints);

        constraints.insets = new Insets(0, 0, vertical, 5);
        constraints.gridwidth = GridBagConstraints.RELATIVE;
        constraints.weightx = 5;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        _progress.setBackground(Color.white);
        AddPanelComponent(_progress, layout, constraints);

        constraints.insets = new Insets(0, 0, vertical, right);
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.weightx = 0;
        _startButton.setFont(boldFont);
        AddPanelComponent(_startButton, layout, constraints);
        _startButton.addActionListener(new DownloadButtonListener());

        _jsessionidCookie = new JSESSIONIDCookie(getParameter("jsessionid"));
        final String stayinAliveInterval = getParameter("keep-alive-interval");
        // Use a default keep-alive interval of 5 if nothing's specified.
        _stayinAliveInterval = stayinAliveInterval == null ? 5 : Integer.parseInt(stayinAliveInterval);
        String showConsoleMessages = getParameter("show-console-messages");
        _showConsoleMessages = showConsoleMessages != null && Boolean.parseBoolean(showConsoleMessages);

        _downloader.start(); //file download thread (consumer)
        _initSessionThread.start(); //session initialization thread (producer)
    }

    private void AddPanelComponent(java.awt.Component comp, GridBagLayout layout, GridBagConstraints con) {
        layout.setConstraints(comp, con);
        this.add(comp);
    }

    /**
     * ***************************************************************************
     * Update labels and buttons
     */
    private synchronized void UpdateStatus(String s) {
        _status.setText(s);
        if (_sessionStatus == SESSION_EMPTY) {
            _startButton.setEnabled(false);
            AllowFolderSelection(false);
            return;
        }
        switch (_downloadStatus) {
            case DOWNLOAD_STOPPED:
                _startButton.setLabel("Start");
                _startButton.setEnabled(true);
                break;
            case DOWNLOAD_FINISHED:
                _startButton.setEnabled(false);
                _browseButton.setEnabled(false);
                break;
            case DOWNLOAD_PROGRESS:
                _startButton.setLabel("Stop");
                _startButton.setEnabled(true);
                break;
        }
    }

    private synchronized void AllowFolderSelection(boolean bAllow) {
        _browseButton.setEnabled(bAllow);
        _localFolderEdit.setEnabled(bAllow);
    }

    /**
     * *************************************************************
     * Synchronized functions working with _files queue
     */
    private synchronized void AddFileToQueue(DownloadFileDescriptor file) {
        _totalCount++;
        _files.add(file);
        if (_totalSize != -1) {
            if (file.getLength() == -1) {
                _totalSize = -1;
            } else {
                _totalSize = _totalSize + file.getLength();
            }
        }
    }

    private synchronized DownloadFileDescriptor RemoveFileFromQueue() {
        if (_files.size() < 1) {
            return null;
        }
        return _files.remove(0);
    }

    private synchronized boolean IsFileQueueEmpty() {
        return (_files.size() < 1);
    }

    private URL buildUrl(String url) {
        try {
            // The catalog we get will have the URLs encoded, so start by decoding them
            url = URLDecoder.decode(url, "UTF-8");

            // Mimic what a browser does, and decode any XML entities we're sent before re-posting to the server
            url = StringEscapeUtils.unescapeXml(url);

            // Now re-encode, but only the parts of the URL that were encoded to begin with
            String[] labelTypes = new String[] {"scans", "resources", "reconstructions", "assessors"};
            for (String labelType : labelTypes) {
                url = urlEncodeItemLabel(url, labelType);
            }

            console("Processing URL: " + url);
            return new URL(url);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String urlEncodeItemLabel(String url, String labelType) throws Exception {
        String  labelRegex = String.format("(?<=/%s/)(.+?)(?=/)", labelType); // e.g. (?<=/scans/){SCAN_ID}(?=/)
        Matcher matcher    = Pattern.compile(labelRegex).matcher(url);
        if (matcher.find() && matcher.groupCount() == 1) {
            return matcher.replaceFirst(URLEncoder.encode(matcher.group(1), "UTF-8"));
        } else {
            return url;
        }
    }

    private void console(String message, Object... objects) {
        if (_showConsoleMessages) {
            System.out.println("*** CONSOLE: " + String.format(message, objects));
        }
    }

    private class DownloadButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == _browseButton) {
                if (_downloader.SelectLocalFolder()) {
                    _localFolderEdit.setText(_saveDirectory.getPath());
                }
            } else //Start/stop button
            {
                if (_downloadStatus == DOWNLOAD_STOPPED) {
                    if (_saveDirectory != null) {
                        _downloadStatus = DOWNLOAD_PROGRESS;
                        _startButton.setLabel("Stop");
                        AllowFolderSelection(false);
                    } else if (_downloader.SelectLocalFolder(_localFolderEdit.getText())) {
                        _downloadStatus = DOWNLOAD_PROGRESS;
                        _startButton.setLabel("Stop");
                        AllowFolderSelection(false);
                    } else {
                        UpdateStatus("Select local folder to start.");
                    }
                } else if (_downloadStatus == DOWNLOAD_PROGRESS) {
                    _downloadStatus = DOWNLOAD_STOPPED;
                    _startButton.setLabel("Resume");
                }
            }

        }
    }

    /**
     * ***************************************************************************
     * Main downloader class
     */

    private static int GZ_THREADS = 0;

    private class Downloader extends Thread {
        final   JFileChooser m_fc               = new JFileChooser();
        private long         m_TotalBytesLoaded = 0;
        private int          m_CurPercent       = 0;

        private GZUnzipperPool pool = new GZUnzipperPool();

        Downloader() {
        }

        public void run() {
            int             sleep_time = 500;
            ExecutorService executor   = null;

            try {
                do {
                    if (_sessionStatus == SESSION_EMPTY) {
                        UpdateStatus("Initializing session...");
                        Thread.sleep(sleep_time);
                        continue;
                    }
                    switch (_downloadStatus) {
                        case DOWNLOAD_PROGRESS:
                            if (_saveDirectory == null) {
                                _downloadStatus = DOWNLOAD_STOPPED;
                                UpdateStatus("Select local folder to save files.");
                                sleep_time = 500;
                                continue;
                            }
                            while (!IsFileQueueEmpty() && _downloadStatus != DOWNLOAD_STOPPED) {
                                if (executor != null && !executor.isShutdown()) {
                                    executor.shutdownNow();
                                }
                                executor = initiateStayinAlive();
                                DownloadNextFile();
                            }
                            if (IsFileQueueEmpty() && _sessionStatus == SESSION_FINISHED && GZ_THREADS == 0) {
                                _downloadStatus = DOWNLOAD_FINISHED;
                                UpdateStatus("Download complete.");
                                _progress.setValue(100);
                                if (executor != null) {
                                    executor.shutdown();
                                }
                            } else if (IsFileQueueEmpty() && _sessionStatus == SESSION_FINISHED) {
                                UpdateStatus("Finishing GZ extraction...");
                                _progress.setValue(99);
                            }
                            sleep_time = 100;
                            break;
                        case DOWNLOAD_STOPPED:
                            sleep_time = 500;
                            break;
                    }
                    Thread.sleep(sleep_time);
                } while (_downloadStatus != DOWNLOAD_FINISHED);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (executor != null && !executor.isShutdown()) {
                    executor.shutdown();
                }
            }
        }

        private ExecutorService initiateStayinAlive() {
            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            executor.scheduleAtFixedRate(new XnatPoker(), _stayinAliveInterval, _stayinAliveInterval, TimeUnit.MINUTES);
            return executor;
        }

        /**
         * ***************************************************************************
         * Opens file selection dialog for local storage.
         */
        boolean SelectLocalFolder() {
            m_fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int res = m_fc.showSaveDialog(DownloaderApplet.this);
            if (res == JFileChooser.APPROVE_OPTION) {
                _saveDirectory = m_fc.getSelectedFile();
                return true;
            }
            return false;
        }

        boolean SelectLocalFolder(String path) {
            if (path.length() < 1) {
                return false;
            }
            _saveDirectory = new File(path);
            return true;
        }

        static final int ZIP_DOWNLOAD = 10 * 1024;

        /**
         * ***************************************************************************
         * Download one file; interrupts if "Stop" is pressed.
         */
        private int DownloadNextFile() {
            if (_saveDirectory == null) {
                return -1;
            }
            if (_currentFile == null) {
                _currentFile = RemoveFileFromQueue();
            }
            if (_currentFile == null) {
                return 0;
            }
            DownloadFileDescriptor row       = _currentFile;
            long                   localSize = 0;
            String                 relative  = row.getDestination();
            if (relative.equals("ZIP")) {
                int count;
                try {
                    URL           url        = buildUrl(row.getSource());
                    URLConnection connection = url.openConnection();
                    _jsessionidCookie.setInRequestHeader(connection);
                    console("Open session with session ID %s to server at %s", _jsessionidCookie.toString(), url.toString());

                    try (final ZipInputStream zis = new ZipInputStream(new BufferedInputStream(connection.getInputStream()))) {
                        byte data[] = new byte[ZIP_DOWNLOAD];

                        ZipEntry entry;
                        while ((entry = zis.getNextEntry()) != null) {
                            if (!entry.isDirectory()) {
                                BufferedOutputStream destination = null;
                                String               name        = entry.getName();
                                localSize += entry.getSize();
                                File f = new File(_saveDirectory, name);
                                console("Servicing zip entry %s, %s bytes compressed to %s, storing in location %s", entry.getName(), formatSize(entry.getSize()), formatSize(entry.getCompressedSize()), f.getAbsolutePath());
                                boolean success = false;
                                try {
                                    f.getParentFile().mkdirs();
                                    // Write the file to the file system
                                    FileOutputStream fos = new FileOutputStream(f);
                                    destination = new BufferedOutputStream(fos, ZIP_DOWNLOAD);
                                    long start = System.currentTimeMillis();
                                    while ((count = zis.read(data, 0, ZIP_DOWNLOAD)) != -1) {
                                        long end = System.currentTimeMillis();
                                        console("Read %s bytes from zip entry %s in %d ms", formatSize(count), entry.getName(), end - start);
                                        start = System.currentTimeMillis();
                                        destination.write(data, 0, count);
                                        destination.flush();
                                        end = System.currentTimeMillis();
                                        console("Wrote %s bytes from zip entry %s in %d ms", formatSize(count), entry.getName(), end - start);

                                        m_TotalBytesLoaded = m_TotalBytesLoaded + count;
                                        console("%s total bytes loaded so far", formatSize(m_TotalBytesLoaded));
                                        if (_sessionStatus != SESSION_FINISHED) //_files are still being added to download queue
                                        {
                                            console("FINISHED! %s total bytes loaded", formatSize(m_TotalBytesLoaded));
                                            UpdateStatus("Downloading, " + m_TotalBytesLoaded / 1024 + " KB completed");
                                        } else {
                                            setProgress();
                                        }
                                        start = System.currentTimeMillis();
                                    }
                                    destination.flush();
                                    success = true;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        if (destination != null) {
                                            destination.close();
                                        }
                                    } catch (Exception ignored) {
                                    }
                                }

                                if (success) {
                                    if (f.getName().endsWith(".gz")) {
                                        pool.addFile(f);
                                    }
                                }
                            } else {
                                File df = new File(_saveDirectory, entry.getName());
                                if (!df.exists()) {
                                    df.mkdirs();
                                }
                                console("Found directory %s, creating in location %s", entry.getName(), df.getAbsolutePath());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    System.err.println("Couldn't find indicated resource, please check URL: " + e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    URL           url        = buildUrl(row.getSource());
                    URLConnection connection = url.openConnection();
                    _jsessionidCookie.setInRequestHeader(connection);
                    InputStream          bis;
                    BufferedOutputStream bos;
                    boolean              bUnzip = false;
                    if (relative.endsWith(".gz")) {
                        relative = relative.substring(0, relative.length() - 3);
                        bUnzip = true;
                    }
                    localSize = row.getLength();
                    File outFile = new File(_saveDirectory, relative);

                    outFile.getParentFile().mkdirs();

                    OutputStream out = new FileOutputStream(outFile);
                    bis = connection.getInputStream();

                    if (bUnzip) {
                        bis = new GZIPInputStream(bis);
                    }
                    bos = new BufferedOutputStream(out);

                    byte[] buff = new byte[10240];
                    int    bytesRead;
                    while ((-1 != (bytesRead = bis.read(buff, 0, buff.length))) && (_downloadStatus == DOWNLOAD_PROGRESS)) {
                        bos.write(buff, 0, bytesRead);
                        bos.flush();
                        if (bUnzip) {
                            _uncompressedTotal = _uncompressedTotal + bytesRead;
                            m_TotalBytesLoaded = m_TotalBytesLoaded + (long) ((double) bytesRead / _compressionRatio);
                        } else {
                            m_TotalBytesLoaded = m_TotalBytesLoaded + bytesRead;
                        }
                        if (_sessionStatus != SESSION_FINISHED) //_files are still being added to download queue
                        {
                            UpdateStatus("Downloading, " + m_TotalBytesLoaded / 1024 + " KB completed, calculating total size...");
                        } else {
                            setProgress();
                        }
                    }
                    bis.close();
                    bos.flush();
                    bos.close();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (_downloadStatus == DOWNLOAD_PROGRESS) {
                _currentFile = null;
                _fileCount++;
                _compressedTotal = _compressedTotal + localSize;
                if (_compressedTotal > 0 && _uncompressedTotal > 0) {
                    _compressionRatio = (double) _uncompressedTotal / (double) _compressedTotal;
                }
                return 1;
            }
            return 0;
        }

        private void setProgress() {
            final int percent;
            if (_totalSize == -1) {
                percent = (int) ((_fileCount * 100) / _totalCount);
                if (m_TotalBytesLoaded > 104857600) //>100MB, display amt of downloaded in MB
                {
                    UpdateStatus("Downloading, " + m_TotalBytesLoaded / 1048576 + " MB completed");
                } else //<100Mb, in Kilobytes
                {
                    UpdateStatus("Downloading, " + m_TotalBytesLoaded / 1024 + " KB completed");
                }
                _progress.setValue(percent);
            } else {
                percent = (int) ((m_TotalBytesLoaded * 100) / _totalSize);
                if (m_CurPercent < percent) {
                    m_CurPercent = percent;
                    if (_totalSize > 104857600) //>100MB, display amt of downloaded in MB
                    {
                        UpdateStatus("Downloading, " + percent + "% completed (" + m_TotalBytesLoaded / 1048576 + " MB out of " + _totalSize / 1048576 + " MB)");
                    } else //<100Mb, in Kilobytes
                    {
                        UpdateStatus("Downloading, " + percent + "% completed (" + m_TotalBytesLoaded / 1024 + " KB out of " + _totalSize / 1024 + " KB)");
                    }
                    _progress.setValue(percent);

                }
            }
        }

        String formatSize(long size) {
            if (size < 1024) {
                return size + " B";
            }
            int exp = (int) (Math.log(size) / Math.log(1024));
            return String.format("%.1f %sB", size / Math.pow(1024, exp), "KMGTPE".charAt(exp - 1));
        }

        private class XnatPoker implements Runnable {
            XnatPoker() {
                _jsContext = getJSContext();
            }

            @Override
            public void run() {
                // Try again in case we missed before.
                if (_jsContext != null) {
                    Object thing = _jsContext.call("handleOk", WE_BELIEVE_IN_NOTHING);
                    if (thing == null) {
                        console(" *** Called handleOk() to keep session alive.");
                    }
                }
            }

            /**
             * Retrieves the Javascript object context if available.
             *
             * @return The Javascript object if available. Returns null if not available (e.g. if running in a debugger or
             * non-Javascript-enabled browser.
             */
            private JSObject getJSContext() {
                final Callable<JSObject> getWindow = new Callable<JSObject>() {
                    public JSObject call() throws JSException {
                        return JSObject.getWindow(DownloaderApplet.this);
                    }
                };
                final ExecutorService es = Executors.newSingleThreadExecutor();
                try {
                    return es.invokeAny(Collections.singleton(getWindow), 10, TimeUnit.SECONDS);
                } catch (Exception e) {
                    return null;
                }
            }

            private final Object[] WE_BELIEVE_IN_NOTHING = new Object[] {};
            private final JSObject _jsContext;
        }
    }//end class Downloader

    private class InitSessionThread extends Thread {
        InitSessionThread() {
        }

        public void run() {
            UpdateStatus("Initializing session...");
            _sessionStatus = SESSION_EMPTY;
            InitSession();
            _sessionStatus = SESSION_FINISHED;
        }

        int nSessions = 0;

        void LoadImages(CatCatalogBean cat) throws MalformedURLException {
            for (Object sub : cat.getSets_entryset()) {
                final CatCatalogBean catalog = (CatCatalogBean) sub;
                LoadImages(catalog);
            }

            for (Object sub : cat.getEntries_entry()) {
                final CatEntryBean entry = (CatEntryBean) sub;

                String format = entry.getFormat();

                final int SESSION_PROGRESS = 1;
                if (format != null && format.equals("CATALOG")) {
                    console("Loading sub-catalog: %s", entry.getUri());
                    XDATXMLReader reader     = new XDATXMLReader();
                    URL           catalogURL = buildUrl(entry.getUri());

                    try {
                        URLConnection connection = catalogURL.openConnection();
                        _jsessionidCookie.setInRequestHeader(connection);
                        BaseElement bean = reader.parse(connection.getInputStream());
                        if (bean instanceof CatCatalogBean) {
                            LoadImages((CatCatalogBean) bean);
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                } else if (format != null && format.equals("ZIP")) {
                    DownloadFileDescriptor s = new DownloadFileDescriptor(entry.getUri(), "ZIP", -1L);
                    AddFileToQueue(s);
                    if (_sessionStatus == SESSION_EMPTY) {
                        _sessionStatus = SESSION_PROGRESS;
                        AllowFolderSelection(true);
                        UpdateStatus("Select local folder.");
                    }
                    nSessions++;
                } else {
                    String relative = entry.getCachepath();
                    Long   size     = null;

                    for (int i = 0; i < entry
                            .getMetafields_metafield().size(); i++) {
                        CatEntryMetafieldBean meta = (CatEntryMetafieldBean) entry
                                .getMetafields_metafield().get(i);
                        if (meta.getName().equals("SIZE")) {
                            String s = meta.getMetafield();
                            size = Long.valueOf(s);
                        }
                    }
                    DownloadFileDescriptor s = new DownloadFileDescriptor(entry.getUri(), relative, size);
                    AddFileToQueue(s);
                    if (_sessionStatus == SESSION_EMPTY) {
                        _sessionStatus = SESSION_PROGRESS;
                        AllowFolderSelection(true);
                        UpdateStatus("Select local folder.");
                    }
                    nSessions++;
                }
            }
        }

        private boolean InitSession() {
            boolean      bRes     = true;
            List<String> sessions = new ArrayList<>();
            int          counter  = 0;
            while (getParameter("session" + counter) != null) {
                sessions.add(getParameter("session" + counter++));
            }
            for (String session : sessions) {
                XDATXMLReader reader = new XDATXMLReader();
                try {
                    URL           catalogURL = buildUrl(session);
                    URLConnection connection = catalogURL.openConnection();
                    _jsessionidCookie.setInRequestHeader(connection);
                    BaseElement bean = reader
                            .parse(connection.getInputStream());
                    if (bean instanceof CatCatalogBean) {
                        LoadImages((CatCatalogBean) bean);
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                    bRes = false;
                }
            }
            return bRes || (nSessions > 0);

        }
    } //end class InitSessionThread

    private class DownloadFileDescriptor {
        DownloadFileDescriptor(final String source, final String destination, final Long length) {
            _source = source;
            _destination = destination;
            _length = length;
        }

        String getSource() {
            return _source;
        }

        String getDestination() {
            return _destination;
        }

        Long getLength() {
            return _length;
        }

        private final String _source;
        private final String _destination;
        private final Long   _length;
    }

    private class GZUnzipperPool {
        final int POOL_SIZE = 5;
        LinkedList<File> ll = new LinkedList<>();

        void addFile(File f) {
            if (GZ_THREADS <= POOL_SIZE) {
                startJob(f);
            } else {
                ll.add(f);
            }
        }

        void startJob(File f) {
            GZ_THREADS++;
            GZUnzipper u = new GZUnzipper(f, this);
            u.start();
        }

        void startThread() {
            if (ll.size() > 0) {
                File f = ll.removeFirst();
                startJob(f);
            }
        }

        synchronized void endThread() {
            if (GZ_THREADS > 0) {
                GZ_THREADS--;
            }
            startThread();
        }
    }

    private class GZUnzipper extends Thread {
        final File           f;
        final GZUnzipperPool pool;

        GZUnzipper(File _f, GZUnzipperPool _pool) {
            f = _f;
            pool = _pool;
        }

        public void run() {
            try {
                InputStream  is      = null;
                OutputStream os      = null;
                File         outFile;
                boolean      success = false;
                try {
                    console(f.getAbsolutePath());
                    is = new BufferedInputStream(new GZIPInputStream(new FileInputStream(f)));

                    outFile = new File(f.getParent(), f.getName().substring(0, f.getName().length() - 3));
                    if (!outFile.exists()) {
                        os = new BufferedOutputStream(new FileOutputStream(outFile));
                        byte[] buf = new byte[1024];

                        int len;
                        while ((len = is.read(buf)) > 0) {
                            os.write(buf, 0, len);
                        }

                        // Complete the entry
                        os.flush();

                        success = true;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    success = false;
                } finally {
                    try {
                        if (os != null) {
                            os.close();
                        }
                    } catch (IOException ignored) {
                    }
                    try {
                        if (is != null) {
                            is.close();
                        }
                    } catch (IOException ignored) {
                    }
                }

                if (success) {
                    f.delete();
                }

            } catch (Exception ignored) {
            } finally {
                pool.endThread();
            }
        }
    }
}//end class DownloaderApplet
